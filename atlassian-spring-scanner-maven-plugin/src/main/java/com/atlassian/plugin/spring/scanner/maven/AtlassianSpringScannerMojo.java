package com.atlassian.plugin.spring.scanner.maven;

import com.atlassian.plugin.spring.scanner.core.AtlassianSpringByteCodeScanner;
import com.atlassian.plugin.spring.scanner.core.ByteCodeScannerConfiguration;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.reflections.util.ClasspathHelper;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.lang.String.format;
import static org.reflections.util.Utils.isEmpty;

/**
 * maven plugin for atlassian-spring-scanning <p> use it by configuring the pom with:
 * <pre>
 * &#60;build>
 *       &#60;plugins>
 *           &#60;plugin>
 *               &#60;groupId>com.atlassian.plugins&#60;/groupId>
 *               &#60;artifactId>atlassian-spring-scanner-maven-plugin&#60;/artifactId>
 *               &#60;version>${project.version}#60;/version>
 *               &#60;executions>
 *                   &#60;execution>
 *                       &#60;goals>
 *                           &#60;goal>atlassian-spring-scanner&#60;/goal>
 *                       &#60;/goals>
 *                       &#60;phase>process-classes&#60;/phase>
 *                   &#60;/execution>
 *               &#60;/executions>
 *               &#60;configuration>
 *                  <... optional configuration here>
 *               &#60;/configuration>
 *           &#60;/plugin>
 *       &#60;/plugins>
 *   &#60;/build>
 * </pre>
 */
@Mojo(name = "atlassian-spring-scanner", defaultPhase = LifecyclePhase.PREPARE_PACKAGE)
public class AtlassianSpringScannerMojo extends AbstractMojo
{
    public static final String OUR_NAME = "Atlassian Spring Byte Code Scanner";
    private static final String DEFAULT_INCLUDE_EXCLUDE = "-java\\..*, -javax\\..*, -sun\\..*, -com\\.sun\\..*";

    @Component
    private MavenProject project;

    @Parameter(defaultValue = DEFAULT_INCLUDE_EXCLUDE)
    private String includeExclude;

    @Parameter(defaultValue = "false")
    private Boolean parallel;

    @Parameter(defaultValue = "false")
    private Boolean verbose;

    @Parameter()
    private List<Dependency> scannedDependencies = new ArrayList<Dependency>();

    public void execute() throws MojoExecutionException, MojoFailureException
    {
        getLog().info("Starting " + OUR_NAME + "...");
        getLog().info("");
        long then = System.currentTimeMillis();

        String outputDirectory = resolveOutputDirectory();
        if (!new File(outputDirectory).exists())
        {
            getLog().warn(format("Skipping because %s was not found", outputDirectory));
            return;
        }

        ByteCodeScannerConfiguration.Builder config = ByteCodeScannerConfiguration.builder()
                .setOutputDirectory(outputDirectory)
                .setClassPathUrls(parseUrls())
                .setIncludeExclude(includeExclude)
                .setLog(makeLogger())
                .setVerbose(verbose);

        // go!
        AtlassianSpringByteCodeScanner scanner = new AtlassianSpringByteCodeScanner(config.build());

        long ms = System.currentTimeMillis() - then;
        getLog().info(format(""));
        getLog().info(format("\tAnalysis ran in %d ms.", ms));
        getLog().info(format("\tEncountered %d total classes", scanner.getStats().getClassesEncountered()));
        getLog().info(format("\tProcessed %d annotated classes", scanner.getStats().getComponentClassesEncountered()));
    }

    private org.slf4j.Logger makeLogger()
    {
        return new MavenLogAdapter(getLog());
    }

    private Set<URL> parseUrls() throws MojoExecutionException
    {
        final Set<URL> urls = Sets.newHashSet();
        urls.add(parseOutputDirUrl());

        if (!isEmpty(includeExclude))
        {
            for (String string : includeExclude.split(","))
            {
                String trimmed = string.trim();
                char prefix = trimmed.charAt(0);
                String pattern = trimmed.substring(1);
                if (prefix == '+')
                {
                    logVerbose(String.format("\tAdding include / exclude %s", prefix));
                    urls.addAll(ClasspathHelper.forPackage(pattern));
                }
            }
        }

        Set<URL> dependencyJars = Sets.newLinkedHashSet();
        for (Artifact artifact : project.getDependencyArtifacts())
        {
            if (isInScannedDeps(artifact))
            {
                logVerbose(String.format("\t(/) Including dependency for scanning %s:%s:%s", artifact.getGroupId(), artifact.getArtifactId(), artifact.getScope()));
                File file = artifact.getFile();
                try
                {
                    URL url = file.toURI().toURL();
                    dependencyJars.add(url);
                }
                catch (MalformedURLException e)
                {
                    getLog().warn(format("Enable to create URL from plugin artifact : %s", file), e);
                }
            }
            else
            {
                logVerbose(String.format("\t\t(X) Ignoring dependency for scanning %s:%s:%s", artifact.getGroupId(), artifact.getArtifactId(), artifact.getScope()));
            }
        }
        urls.addAll(dependencyJars);

        if (dependencyJars.size() > 0)
        {
            getLog().info(format("\t(/) The following dependencies will also be scanned for annotations : "));
            getLog().info(format(""));
            for (URL jar : dependencyJars)
            {
                getLog().info(format("\t\t%s", jar));
            }
        }

        return urls;
    }

    private void logVerbose(String message)
    {
        if (verbose)
        {
            getLog().info(message);
        }
    }

    private boolean isInScannedDeps(final Artifact artifact)
    {
        if (!isSensibleScope(artifact))
        {
            return false;
        }
        Predicate<Dependency> matchesGA = new Predicate<Dependency>()
        {
            @Override
            public boolean apply(final Dependency input)
            {
                return artifact.getGroupId().equals(input.getGroupId()) && artifact.getArtifactId().equals(input.getArtifactId());
            }
        };
        return Iterables.any(scannedDependencies, matchesGA);
    }

    private boolean isSensibleScope(final Artifact artifact)
    {
        return !Artifact.SCOPE_TEST.equals(artifact.getScope());
    }

    private URL parseOutputDirUrl() throws MojoExecutionException
    {
        try
        {
            File outputDirectoryFile = new File(resolveOutputDirectory() + '/');
            return outputDirectoryFile.toURI().toURL();
        }
        catch (MalformedURLException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private String resolveOutputDirectory()
    {
        return getProject().getBuild().getOutputDirectory();
    }

    public MavenProject getProject()
    {
        return project;
    }
}
