package com.atlassian.plugin.spring.scanner.processor;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.core.SpringIndexWriter;
import com.atlassian.plugin.spring.scanner.util.CommonConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

/**
 * The base class for all processors that need to write class index files
 */
public abstract class IndexWritingAnnotationProcessor extends AbstractProcessor implements Processor
{
    private static boolean invoked = false;
    private SpringIndexWriter springIndexWriter;
    private long msTaken = 0;

    @Override
    public synchronized void init(final ProcessingEnvironment processingEnv)
    {
        super.init(processingEnv);
        springIndexWriter = new SpringIndexWriter(processingEnv.getFiler());
    }

    @Override
    public SourceVersion getSupportedSourceVersion()
    {
        return SourceVersion.latest();

    }

    protected void doProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv, String indexKey)
    {
        if (!invoked)
        {
            println(String.format("Starting Atlassian Spring Java Annotations Scanner..."));
            invoked = true;
        }

        long then = System.currentTimeMillis();
        try
        {
            for (TypeElement anno : annotations)
            {
                Set<? extends Element> elementsAnnotatedWith = roundEnv.getElementsAnnotatedWith(anno);
                for (Element element : elementsAnnotatedWith)
                {
                    //we delegate to the subclasses for this as they may need special handling based on the kind of element
                    TypesAndAnnotation typesAndAnnotation = getTypeAndAnnotation(element, anno);
                    if (null == typesAndAnnotation || null == typesAndAnnotation.getTypeElement())
                    {
                        continue;
                    }

                    //get the bean name specified on the annotation (as it's value) if any
                    String annotationClassName = anno.getQualifiedName().toString();
                    String nameFromAnnotation = nameFromAnnotation(typesAndAnnotation);
                    //Build the string to put in the index file. Always starts with fully qualified class name
                    String classTypeName = resolveClassTypeName(typesAndAnnotation);

                    Set<String> profiles = getProfiles(typesAndAnnotation.getContainingClassTypeElement());
                    springIndexWriter.encounteredAnnotation(profiles, annotationClassName, nameFromAnnotation, classTypeName);

                    checkForModuleType(profiles, typesAndAnnotation);
                }
            }

            if (!roundEnv.processingOver())
            {
                msTaken += (System.currentTimeMillis() - then);
                return;
            }

            springIndexWriter.writeIndexes();

            msTaken += (System.currentTimeMillis() - then);

            println(String.format("\tAtlassian Spring Scanner '%s' index file written in %s ms", indexKey, msTaken));

        }
        catch (RuntimeException e)
        {
            throw new RuntimeException(e);
        }
    }

    private String resolveClassTypeName(final TypesAndAnnotation typesAndAnnotation)
    {
        TypeElement typeElement = typesAndAnnotation.getTypeElement();
        ElementKind kind = typeElement.getKind();
        if (! (kind.isClass() || kind.isInterface()))
        {
            throw new IllegalStateException("We can only record classes / interfaces at this point.  Something is seriously wrong with this code given '" + kind.toString() + "'");
        }
        if (typeElement.getEnclosingElement().getKind() == ElementKind.PACKAGE)
        {
            // shortcut - we have no enclosing type
            return typeElement.getQualifiedName().toString();
        }
        Element element = typeElement;
        Stack<String> classNames = new Stack<String>();
        while (true)
        {
            Element enclosingElement = element.getEnclosingElement();
            kind = enclosingElement.getKind();
            if (kind.isClass() || kind.isInterface())
            {
                Element grandParentElement = enclosingElement.getEnclosingElement();
                if (grandParentElement.getKind() == ElementKind.PACKAGE)
                {
                    StringBuilder sb = new StringBuilder(asTypeElement(enclosingElement).getQualifiedName().toString());
                    sb.append("$");
                    sb.append(element.getSimpleName());
                    while (!classNames.isEmpty())
                    {
                        sb.append("$");
                        sb.append(classNames.pop());
                    }
                    return sb.toString();
                }
                classNames.push(element.getSimpleName().toString());
            }
            else
            {
                throw new IllegalStateException("We have not handled the type hierarchy case of " + kind.toString());
            }
            element = enclosingElement;
        }
    }

    private TypeElement asTypeElement(final Element element)
    {
        return (TypeElement) processingEnv.getTypeUtils().asElement(element.asType());
    }

    private void checkForModuleType(final Set<String> profiles, final TypesAndAnnotation typesAndAnnotation)
    {
        ModuleType annotation = typesAndAnnotation.getTypeElement().getAnnotation(ModuleType.class);
        if (annotation != null)
        {
            //
            // if we have an @ModuleType component then we need to slip in a special fix up
            // component for the host container.  This simplifies how to make a module type factory
            // that used to happen via <module-type>
            //
            springIndexWriter.encounteredAnnotation(profiles, Component.class.getCanonicalName(), "", CommonConstants.HOST_CONTAINER_CLASS);
        }
    }

    private String nameFromAnnotation(final TypesAndAnnotation typesAndAnnotation)
    {
        String nameFromAnnotation = "";
        try
        {
            Annotation componentAnnotation = typesAndAnnotation.getAnnotation();
            Method valueMethod = componentAnnotation.getClass().getDeclaredMethod("value");
            nameFromAnnotation = (String) valueMethod.invoke(componentAnnotation);
        }
        catch (NoSuchMethodException e)
        {
            //ignore
        }
        catch (InvocationTargetException e)
        {
            //ignore
        }
        catch (IllegalAccessException e)
        {
            //ignore
        }
        return nameFromAnnotation;
    }

    private Set<String> getProfiles(final TypeElement typeElement)
    {
        Set<String> profileNames = new HashSet<String>();

        addProfileNames(typeElement.getAnnotationMirrors(), profileNames);
        // if we have specific profile information on the class then these override the package-info
        if (!profileNames.isEmpty())
        {
            return profileNames;
        }
        Element enclosingElement = typeElement.getEnclosingElement();
        if (enclosingElement != null && enclosingElement.getKind() == ElementKind.PACKAGE)
        {
            List<? extends AnnotationMirror> annotationMirrors = enclosingElement.getAnnotationMirrors();
            addProfileNames(annotationMirrors, profileNames);
        }
        return profileNames;

    }

    private void addProfileNames(final List<? extends AnnotationMirror> annotationMirrors, final Set<String> profileNames)
    {
        for (AnnotationMirror annotationMirror : annotationMirrors)
        {
            String annotationName = annotationMirror.getAnnotationType().toString();
            if (Profile.class.getCanonicalName().equals(annotationName) || "com.spring.shit.Profile".equals(annotationName))
            {
                Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = annotationMirror.getElementValues();
                for (AnnotationValue value : elementValues.values())
                {
                    String annotationValue = String.valueOf(value.getValue());
                    // for some reason there are quotes on the start and end of the annotation value
                    profileNames.add(removeQuotes(annotationValue));
                }
            }
        }
    }

    private String removeQuotes(final String annotationValue)
    {
        return StringUtils.removeStart(StringUtils.removeEnd(annotationValue, "\""), "\"");
    }

    private void println(String msg)
    {
        System.out.println(msg);
    }

    /**
     * Returns the fully qualified class name of the thing that's annotated as well as the instance of the annotation.
     * This delegates to subclasses as they may need to have special handling of this based on the kind of element we're
     * dealing with.
     *
     * @param element the compiled class element
     * @param annotation the annotation in play
     * @return a combination of those two if applicable
     */
    public abstract TypesAndAnnotation getTypeAndAnnotation(Element element, TypeElement annotation);

    protected TypeElement getContainingClass(final Element element)
    {
        Element enclosingElement = element;
        while (enclosingElement.getKind() != ElementKind.CLASS)
        {
            enclosingElement = enclosingElement.getEnclosingElement();
            if (enclosingElement == null)
            {
                break;
            }
        }
        if (enclosingElement == null)
        {
            throw new IllegalStateException("Unable to find an enclosing class for : " + element.getSimpleName());
        }
        return (TypeElement) processingEnv.getTypeUtils().asElement(enclosingElement.asType());
    }

    /**
     * This holder class is used to contain the annotation encountered, the type of the class that must be written to
     * the index for that annotation and the containing class type that annotation was found on.
     */
    protected class TypesAndAnnotation
    {
        private final TypeElement typeElement;
        private final TypeElement containingClassTypeElement;
        private final Annotation annotation;

        protected TypesAndAnnotation(TypeElement typeElement, final TypeElement containingClassTypeElement, Annotation annotation)
        {
            this.typeElement = typeElement;
            this.containingClassTypeElement = containingClassTypeElement;
            this.annotation = annotation;
        }

        public TypeElement getTypeElement()
        {
            return typeElement;
        }

        public Annotation getAnnotation()
        {
            return annotation;
        }

        public TypeElement getContainingClassTypeElement()
        {
            return containingClassTypeElement;
        }

        @Override
        public String toString()
        {
            return containingClassTypeElement.getQualifiedName() + " containing " + typeElement.getQualifiedName() + " for " + annotation;
        }
    }
}
