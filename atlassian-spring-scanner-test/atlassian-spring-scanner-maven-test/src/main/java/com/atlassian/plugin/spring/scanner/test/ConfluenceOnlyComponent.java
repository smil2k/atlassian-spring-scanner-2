package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.ConfluenceComponent;

/**
 * A component that should only be in Confluence
 */
@ConfluenceComponent
public class ConfluenceOnlyComponent
{
}
