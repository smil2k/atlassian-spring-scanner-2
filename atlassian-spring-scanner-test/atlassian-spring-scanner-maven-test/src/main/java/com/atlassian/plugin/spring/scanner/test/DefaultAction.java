package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.io.PrintWriter;

/**
 */
public class DefaultAction extends JiraWebActionSupport
{
    private final ConsumingMixedComponents consumingMixedComponents;
    private PrintWriter out;

    public DefaultAction(final ConsumingMixedComponents consumingMixedComponents)
    {
        this.consumingMixedComponents = consumingMixedComponents;
    }

    @Override
    protected String doExecute() throws Exception
    {
        out = getHttpResponse().getWriter();

        out.print("<html><body>");

        out.print("<h1>Default Action</h1>"
                + "<div>"
                + "You should only be able to see this action IF the child container is in action "
                + "otherwise you will get a 404 bean wiring error!"
                + "</div>");

        out.print("</body></html>");
        getHttpResponse().flushBuffer();
        return NONE;
    }
}
