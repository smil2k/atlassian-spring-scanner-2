package com.atlassian.plugin.spring.scanner.test;

public interface ExposedAsASecondComponentInterface
{
    void doOtherStuff();
}
