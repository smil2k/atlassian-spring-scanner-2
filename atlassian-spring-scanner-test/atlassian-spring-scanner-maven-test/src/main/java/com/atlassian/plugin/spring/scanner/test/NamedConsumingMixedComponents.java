package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NamedConsumingMixedComponents
{
    private final CommentService issueService;
    private final InternalComponent internalComponent;
    private final InternalComponent2 internalComponent2;

    @Autowired
    public NamedConsumingMixedComponents(@ComponentImport final CommentService commentService, final InternalComponent internalComponent, final InternalComponent2 internalComponent2)
    {
        this.issueService = commentService;
        this.internalComponent = internalComponent;
        this.internalComponent2 = internalComponent2;
    }
}
