package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.StashComponent;

/**
 * A component that should only be in Stash
 */
@StashComponent
public class StashOnlyComponent
{
}
