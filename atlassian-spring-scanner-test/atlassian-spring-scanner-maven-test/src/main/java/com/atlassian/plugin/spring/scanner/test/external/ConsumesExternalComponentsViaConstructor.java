package com.atlassian.plugin.spring.scanner.test.external;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class ConsumesExternalComponentsViaConstructor
{
     final ExternalJarComponentComposite externalJarComponentComposite;

    @Autowired
    public ConsumesExternalComponentsViaConstructor(final @ClasspathComponent ExternalJarComponentComposite externalJarComponentComposite)
    {
        this.externalJarComponentComposite = externalJarComponentComposite;
    }
}
